def get_eratosfen_list(n):
    n += 1

    a = [True]*n
    a[0] = a[1] = False

    for k in range(2, n):
        if a[k]:
            for m in range(2*k, n, k):
                a[m] = False

    return a


def get_eratosfen_sublist(a, b):
    # возвращает вырезанный кусок списка в значениях True/False
    main_eratosfen_list = get_eratosfen_list(1000000)
    lst = []
    for x in range(a, b):
        lst.append(main_eratosfen_list[x])
    return lst


def get_eratosfen_sublist_as_numbers(a, b):
    # возвращает вырезанный кусок списка - в нем только простые числа
    main_eratosfen_list = get_eratosfen_list(1000000)
    lst = []
    for x in range(a, b):
        if main_eratosfen_list[x]:
            lst.append(x)
    return lst


def number_to_canvas_topleftcorner_xy(n, canvas_width, canvas_height):
    # возвращает координаты левого верхнего угла, сответвтующего простому числу для построения прямоугольника на canvas
    if n <= 0:
        return [0, 0]
    if n >= canvas_width * canvas_height:
        return [canvas_width-1, canvas_height-1]

    x = n % canvas_width
    y = n // canvas_width
    return [x, y]


def listnumbers_to_listxy(canvas_width, canvas_height):
    # пробразует список простых чисел в список вида - [ [x1, y1], [x2, y2] ... ]
    lst_result = []
    lst_numbers = get_eratosfen_sublist_as_numbers(0, canvas_width*canvas_height)
    for elem in lst_numbers:
        lst_result.append(number_to_canvas_topleftcorner_xy(elem, canvas_width, canvas_height))
    return lst_result


def main():
    w = 7
    h = 4
    print(listnumbers_to_listxy(w, h))


if __name__ == '__main__':
    main()
