import time


def timer(func):
    def wrapper(*args, **kwargs):
        before_sec = time.time()
        func(*args, **kwargs)
        after_sec = time.time()
        val_sec = after_sec - before_sec
        # val_mil_sec = int((after_sec - before_sec).total_seconds() * 1000)
        print("Function took:", val_sec, "seconds")
        print()

    return wrapper


# @timer
# def run1():
#     a = 2 + 3
#
#
# @timer
# def run():
#     time.sleep(2)
#
#
# run()

