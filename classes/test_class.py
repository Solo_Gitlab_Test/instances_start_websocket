class myEratosfen:
    # def __init__(self, *args, **kwargs):
    #     pass

    def __init__(self, *args, **kwargs):
        if kwargs.get('canvas_width') != kwargs.get('canvas_height'):
            self.list_count = 25
            return
        if kwargs.get('list_count') is not None:
            self.list_count = kwargs.get('list_count')
        elif kwargs.get('canvas_width') is not None and kwargs.get('canvas_height') is not None:
            self.list_count = kwargs.get('canvas_width')*kwargs.get('canvas_height')
        else:
            self.list_count = 25

        self.eratosfen_list = self.__get_eratosfen_list(self.list_count)

    def __get_eratosfen_list(self, n):
        n += 1

        a = [True]*n
        a[0] = a[1] = False

        for k in range(2, n):
            if a[k]:
                for m in range(2*k, n, k):
                    a[m] = False

        return a


def main():
    er = myEratosfen(canvas_width=5, canvas_height=5)
    print(er.list_count)
    print(er.eratosfen_list)


if __name__ == '__main__':
    main()

