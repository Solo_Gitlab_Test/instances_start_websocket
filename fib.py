from execution_speed import timer


def fib(n):
    if n <= 1:
        return n
    return fib(n-1) + fib(n-2)


@timer
def fib_optimized_dict(n):
    d = dict()
    f = [0, 1] + [0] * (n-1)
    for i in range(2, n+1):
        f[i] = f[i-1] + f[i-2]
    d['fibo_list'] = f
    d['fibo_n'] = f[n]
    return d


if __name__ == "__main__":
    print(fib_optimized_dict(10))
    print(fib(10))




