import aiohttp
import aiohttp_jinja2
from aiohttp import web
from faker import Faker
from eratosfen import *


def get_random_name():
    fake = Faker()
    return fake.name()


async def index(request):
    ws_current = web.WebSocketResponse()
    ws_ready = ws_current.can_prepare(request)
    if not ws_ready.ok:
        print('===========================socket prepared not ok')
        return aiohttp_jinja2.render_template('index.html', request, {})

    await ws_current.prepare(request)
    print('===========================socket prepared ok')

    name = get_random_name()
    # log.info('%s joined.', name)

    await ws_current.send_json({'action': 'connect', 'name': name})

    for ws in request.app['websockets'].values():
        await ws.send_json({'action': 'join', 'name': name})
    request.app['websockets'][name] = ws_current

    while True:
        msg = await ws_current.receive()

        if msg.type == aiohttp.WSMsgType.text:
            if msg.data == 'get_eratosren':
                print('get_eratosren received')
                await ws_current.send_json({'action': 'eratosfen_data', 'name': '12345'})
                print('get_eratosren message was sent ... ')

            if msg.data == 'draw_eratosfen':
                print('draw_eratosfen received')
                await ws_current.send_json({'action': 'draw_eratosfen_result', 'name': listnumbers_to_listxy(500, 500)})
                print('draw_eratosfen_result message was sent ... ')

            for ws in request.app['websockets'].values():
                if ws is not ws_current:
                    await ws.send_json(
                        {'action': 'sent', 'name': name, 'text': msg.data})
        else:
            break

    del request.app['websockets'][name]
    # log.info('%s disconnected.', name)
    for ws in request.app['websockets'].values():
        await ws.send_json({'action': 'disconnect', 'name': name})

    return ws_current

