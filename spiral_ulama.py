from eratosfen import *


def print_blue(text):
    print("\033[34m {}" .format(text), end='\t')


def print_normal(text):
    print("\033[0m {}" .format(text), end='\t')


def list_test():
    a = [[1, 2, 3, 4], [5, 6], [7, 8, 9]]
    for i in range(len(a)):
        for j in range(len(a[i])):
            print(str(a[i][j]) + '(' + str(j) + ')', end=' ')
        print()


def list_test_reverse():
    a = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    for i in range(len(a)-1, 3, -1):
        print(a[i])


def list_gen(n, m):
    # возвращает список списков где элементы все = 0, n - кол-во строк, m - столбцов
    return [[0 for j in range(m)] for i in range(n)]


def print_list(a):
    l = len(a) * len(a[0])
    er = get_eratosfen_list(l)

    for i in range(len(a)):
        for j in range(len(a[i])):
            if er[a[i][j]]:
                print_blue(str(a[i][j]))
            else:
                print_normal(str(a[i][j]))
        print()
        print()


def list_fill(a):
    l = len(a) * len(a[0])
    print('l = {}'.format(l))
    er = get_eratosfen_list(l)
    print('er length = {}'.format(len(er)))
    # print(er)
    print('============')

    k = 1
    n = 1
    i = len(a) // 2
    j = len(a) // 2
    a[i][j] = n

    while n <= l:
        for m in range(k):
            n += 1
            j += 1

            if n > l:
                break

            a[i][j] = n

        if n > l:
            continue

        for m in range(k):
            n += 1
            i -= 1
            a[i][j] = n

        k += 1

        for m in range(k):
            n += 1
            j -= 1
            a[i][j] = n

        for m in range(k):
            n += 1
            i += 1
            a[i][j] = n

        k += 1

    return a


def main():
    try:
        a = list_gen(13, 13)
        a = list_fill(a)
        print_list(a)
    except IndexError:
        print('введите в list_gen нечетные числа, например list_gen(21, 21)')

if __name__ == '__main__':
    main()
